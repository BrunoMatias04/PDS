#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>
/*Estrutura contendo todos os atributos para Esportes*/
struct Atributos{
	char nome[30];
	char data[10];
	int codigo;
	int titulos;
	char estado[3];
	char presidente[30];
};
typedef struct Atributos info;
/*Estrutura contendo um "item" da uma lista encadeada*/
struct _no{
	info informacao;
	struct _no *prox;
	struct _no *ant;

};
typedef struct _no no;
/*Cabeçalho para uma lista encadeada*/
struct cab{
	int qtde;
	no *inicio;
	no *final;
};
typedef struct cab lista;
