#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>

void SalvarUolmail(listaUolmail *l){
	system("clear");
	char h;
	printf("\n'Deseja salvar alterações? [s] para sim , [n] para não'\n");
	h = getchar();
	__fpurge(stdin);
	if(h == 's'){
	FILE *arq;
	int ret;
	int i = 0;
	char c;
	noUolmail *tmp = l->final;
	arq = fopen("ArquivoUolmail.dat","w");
	infoUolmail infos[l->qtde];
	while(tmp != NULL){
		infos[i] = tmp->informacao;
		tmp = tmp->ant;
		i++;
	}
	if(arq != NULL){
			ret = fwrite(infos,sizeof(infoUolmail),l->qtde,arq);
			printf("\nForam gravados %d registros\n",ret);
			fclose(arq);
	}
	else
		printf("Erro:abertura no arquivo");
	printf("\n\n'Aperte a tecla [ENTER] para voltar ao menu.'\n");
	c = getchar();
	__fpurge(stdin);
	}
}
void RecuperarUolmail(listaUolmail *l){
	while(l->inicio != NULL){
		RemoverInicioUolmail(l);
	}
	FILE *arq;
	int i = 0,ret;
	infoUolmail infos[30];
	arq = fopen("ArquivoUolmail.dat","r");
	if(arq != NULL){
		ret = fread(infos,sizeof(infoUolmail),30,arq);
		l->qtde = ret;
	}
	while(i < l->qtde){
		noUolmail *p = (noUolmail*)malloc(sizeof(noUolmail));
		p->informacao = infos[i];
		p->ant = NULL;
		p->prox = l->inicio;
		l->inicio = p;
		if(l->final == NULL){
			l->final = l->inicio;
		}
		else
			l->inicio->prox->ant = p;
		i++;
	}
}

