#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>

void InserirListaCarros(listaCarros *l){
	noCarros *p = (noCarros*)malloc(sizeof(noCarros));
	printf("\nModelo do Carro: ");
	__fpurge(stdin);
	LerStringCarros(p->informacao.modelo);
	__fpurge(stdin);
	p->informacao.codigo = l->qtde;
	printf("\nCodigo do Carro: %d\n",p->informacao.codigo);
	printf("\nAno: ");
	__fpurge(stdin);
	fgets(p->informacao.ano,11,stdin);
	printf("\nNumero de Portas: ");
	__fpurge(stdin);
	scanf("%d",&p->informacao.numero_portas);
	__fpurge(stdin);
	printf("\nCor: ");
	fgets(p->informacao.cor,10,stdin);
	__fpurge(stdin);
	printf("\nMotor: ");
	fgets(p->informacao.motor,30,stdin);
	__fpurge(stdin);
	p->prox = l->inicio;
	p->ant = NULL;
	l->inicio = p;
	if(l->final == NULL)
		l->final = l->inicio;
	else
		l->inicio->prox->ant = p;
}
void ImprimeTodosCarros(listaCarros *l){
	noCarros *p = l->inicio;
	while(p != NULL){
		printf("--------------------------------------------------------------------------------");
		printf("\n%d  -  %s\n",p->informacao.codigo,p->informacao.modelo);
		p = p->prox;
	}
	printf("---------------------------------------------------------------------------------");
	printf("\n");
}
void ImprimeEspecificoCarros(listaCarros *l){
	noCarros *tmp = l->inicio;
	char c[30];
	char n;
	int escolha;
	printf("\nGostaria de pesquisar por:\n1-Nome do Carro.\n2-Codigo do Carro.\n");
	printf("Escolha: ");
	scanf("%d",&escolha);
	__fpurge(stdin);
	if(escolha == 1){
		printf("\nDigite o Modelo do Carro desejado: ");
		LerStringCarros(c);
		__fpurge(stdin);
		int i = 0;
		int j = 0;
		while(c[i] != '\0'){
			i++;
		}
		int k = 0;
		while(tmp != NULL){
			j = 0;
			while(j < i){
				if(tmp->informacao.modelo[k] == c[k])
					k++;
			j++;
			}
			if(k == i){
				printf("\n%d - %s\n",tmp->informacao.codigo,tmp->informacao.modelo);
				printf("\nAno: %s",tmp->informacao.ano);
				printf("\nNumero de Portas: %d",tmp->informacao.numero_portas);
				printf("\n\nCor: %s",tmp->informacao.cor);
				printf("\nMotor: %s",tmp->informacao.motor);
				break;
			}
			tmp = tmp->prox;
		}
	}
	else if(escolha == 2){
		int codigo;
		printf("\nCodigo do Carro: ");
		scanf("%d",&codigo);
		__fpurge(stdin);
		while(tmp != NULL){
			if(tmp->informacao.codigo == codigo){
				printf("\n%d - %s\n",tmp->informacao.codigo,tmp->informacao.modelo);
				printf("\nAno: %s",tmp->informacao.ano);
				printf("\nNumero de Portas: %d",tmp->informacao.numero_portas);
				printf("\n\nCor: %s",tmp->informacao.cor);
				printf("\nMotor: %s",tmp->informacao.motor);
			}
			tmp = tmp->prox;
		}
	}
	printf("\n\nAperte a tecla [ENTER] para voltar ao menu.\n");
	n = getchar();
	__fpurge(stdin);
}
void RemoverCarros(listaCarros *l){
	noCarros *tmp = l->inicio;
	noCarros *p;
	printf("\nREMOVER - Codigo do Carro: ");
	char c[30];
	LerStringCarros(c);
    	__fpurge(stdin);
	int i = 0;
	int j = 0;
	if(l->qtde == 0){
        l->inicio = NULL;
        l->final = NULL;
        free(tmp);
	}
	while(c[i] != '\0'){
		i++;
	}
	int k = 0;
	while(tmp != NULL){
		j = 0;
		while(j < i){
			if(tmp->informacao.modelo[k] == c[k])
				k++;
		j++;
		}
		if(k == i){
			p = tmp;
			if(tmp == l->inicio){
				tmp->prox->ant = NULL;
				l->inicio = tmp->prox;
				free(p);
			}
			else if(tmp == l->final){
				tmp->ant->prox = NULL;
				l->final = tmp->ant;
				free(p);
			}
			else{
				tmp->ant->prox = tmp->prox;
				tmp->prox->ant = tmp->ant;
				free(p);
			}
			break;
		}

		tmp = tmp->prox;
	}
}
void MudarInfoCarros(listaCarros *l){
	printf("\nALTERACOES - Modelo do Carro: ");
	char c[30];
	noCarros *tmp = l->inicio;
	int escolha;
	LerStringCarros(c);
	__fpurge(stdin);
	char g;
	int i = 0;
	int j = 0;
	while(c[i] != '\0'){
		i++;
	}
	int k = 0;
	while(tmp != NULL){
		j = 0;
		while(j < i){
			if(tmp->informacao.modelo[k] == c[k])
				k++;
		j++;
		}
		if(k == i){
			while(1){
				printf("\nQual campo deseja alterar:\n\n1-Modelo.\n2-Ano.\n3-Numero de Portas.\n4-Cor.\n5-Motor.\n");
				printf("->");
				scanf("%d",&escolha);
				__fpurge(stdin);
				if(escolha == 1){
					printf("\nDigite o novo Modelo desejado:");
					LerStringCarros(tmp->informacao.modelo);
					__fpurge(stdin);
				}
				if(escolha == 2){
					printf("\nDigite o novo ano desejado:");
					fgets(tmp->informacao.ano,10,stdin);
					__fpurge(stdin);
				}
				if(escolha == 3){
					printf("\nDigite o novo Numero de Portas desejada:");
					scanf("%d",&tmp->informacao.numero_portas);
					__fpurge(stdin);
				}
				if(escolha == 4){
					printf("\nDigite a nova cor desejado:");
					fgets(tmp->informacao.cor,10,stdin);
					__fpurge(stdin);
				}
				if(escolha == 5){
					printf("\nDigite o novo motor desejado:");
					fgets(tmp->informacao.motor,30,stdin);
					__fpurge(stdin);
				}
			system("cls");
			printf("\nDeseja Alterar outro campo?\n\nDigite [s] para ""sim"" e [n] para ""nao""\n");
			scanf("%c",&g);
			if(g == 'n')
				break;
			}
			break;
		}
		tmp = tmp->prox;
	}
}
void RemoverInicioCarros(listaCarros *l){
	noCarros *tmp = l->inicio;
	if(l->inicio == l->final){
		l->inicio = NULL;
		l->final = NULL;
	}
	else{
		l->inicio->prox->ant = NULL;
		l->inicio = l->inicio->prox;
	}
	free(tmp);
}
