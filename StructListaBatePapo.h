#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>

struct Atributos_batepapo{
	char nome_sala[30];
	char categoria[30];
	int codigo;
	int max_pessoas;
};
typedef struct Atributos_batepapo infoBatePapo;
struct _noBatePapo{
	infoBatePapo informacao;
	struct _noBatePapo *prox;
	struct _noBatePapo *ant;

};
typedef struct _noBatePapo noBatePapo;
struct cabBatePapo{
	int qtde;
	noBatePapo *inicio;
	noBatePapo *final;
};
typedef struct cabBatePapo listaBatePapo;
