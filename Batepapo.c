
#include "StructListaBatePapo.h"
#include "FuncoesGeraisBatePapo.h"
#include "FuncoesListaBatePapo.h"
#include "FuncoesSalvar_e_RecuperarBatePapo.h"

int BatePapo(){
	int escolha;
	listaBatePapo *l = (listaBatePapo*)malloc(sizeof(listaBatePapo));
	l->qtde = 0;
	l->inicio = NULL;
	l->final = NULL;
	Recuperar_batepapo(l);
	while(1){
		system("clear");
		printf("\n1-Cadastrar Sala.\n2-Consultar Sala.\n3-Alterar informações da Sala.\n4-Remover Sala.\n5-Voltar ao menu\n\n");
		printf("Escolha uma opção: ");
		scanf("%d",&escolha);
		__fpurge(stdin);
		if(escolha == 1){
			system("clear");
			l->qtde++;
			InserirLista_batepapo(l);
			Salvar_batepapo(l);
			Recuperar_batepapo(l);
		}
		else if(escolha == 4){
			system("clear");
			l->qtde--;
			Remover_batepapo(l);
			Salvar_batepapo(l);
			Recuperar_batepapo(l);
		}
		else if(escolha == 3){
			system("clear");
			ImprimeTodos_batepapo(l);
			MudarInfo_batepapo(l);
			Salvar_batepapo(l);
			Recuperar_batepapo(l);
		}
		else if(escolha == 2){
			system("clear");
			ImprimeTodos_batepapo(l);
			ImprimeEspecifico_batepapo(l);
		}
		else if(escolha == 5){
			break;
		}
	}
	return 0;
}
