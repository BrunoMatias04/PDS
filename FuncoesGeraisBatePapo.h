#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>

void LerString_batepapo(char *Nome)
{
    char c;
    int i=0;
    do
    {
        c = getchar();
	if(c == '\n'){
		break;
	}
        Nome[i] = c;
        i++;
    }while(c != '\n');
	Nome[i] = '\0';
}
