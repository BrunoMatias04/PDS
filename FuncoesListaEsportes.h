#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>
/*Coloca na lista do parametro da funçao um novo nó com todos os seus atributos*/
void InserirLista(lista *l){
	no *p = (no*)malloc(sizeof(no));
	printf("\nNome do time:");
	__fpurge(stdin);
	LerString(p->informacao.nome);
	__fpurge(stdin);
	p->informacao.codigo = l->qtde;
	printf("\nCodigo do time é: %d\n",p->informacao.codigo);
	printf("\nData de Fundação:");
	__fpurge(stdin);
	fgets(p->informacao.data,10,stdin);
	printf("\nTitulos conquistados:");
	__fpurge(stdin);
	scanf("%d",&p->informacao.titulos);
	__fpurge(stdin);
	printf("\nEstado de origem:");
	fgets(p->informacao.estado,3,stdin);
	__fpurge(stdin);
	printf("\nPresidente do clube:");
	fgets(p->informacao.presidente,30,stdin);
	__fpurge(stdin);
	p->prox = l->inicio;
	p->ant = NULL;
	l->inicio = p;
	if(l->final == NULL)
		l->final = l->inicio;
	else
		l->inicio->prox->ant = p;
}
/*Imprime a lista dada no parametro um por um*/
void ImprimeTodos(lista *l){
	no *p = l->inicio;
	while(p != NULL){
		printf("--------------------------------------------------------------------------------");
		printf("\n%d  -  %s\n",p->informacao.codigo,p->informacao.nome);
		p = p->prox;
	}
	printf("--------------------------------------------------------------------------------");
	printf("\n");
}
/* Pesquisa um item da lista e imprime todos os seus atributos*/
void ImprimeEspecifico(lista *l){
	no *tmp = l->inicio;
	char c[30];
	char n;
	int escolha;
	printf("\nGostaria de pesquisar por:\n1-Nome do time.\n2-Codigo do Time.\n");
	printf("Escolha:");
	scanf("%d",&escolha);
	__fpurge(stdin);
	if(escolha == 1){
		printf("\nDigite o nome do time desejado:");
		LerString(c);
		__fpurge(stdin);
		int i = 0;
		int j = 0;
		while(c[i] != '\0'){
			i++;
		}
		int k = 0;
		while(tmp != NULL){
			j = 0;
			while(j < i){
				if(tmp->informacao.nome[k] == c[k])
					k++;
			j++;
			}
			if(k == i){
				printf("\n%d - %s\n",tmp->informacao.codigo,tmp->informacao.nome);
				printf("\nData:%s",tmp->informacao.data);
				printf("\nTitulos:%d",tmp->informacao.titulos);
				printf("\n\nEstado:%s",tmp->informacao.estado);
				printf("\n\nPresidente:%s",tmp->informacao.presidente);
				break;
			}
			tmp = tmp->prox;
		}
	}
	else if(escolha == 2){
		int codigo;
		printf("\nDigite o codigo do time desejado:");
		scanf("%d",&codigo);
		__fpurge(stdin);
		while(tmp != NULL){
			if(tmp->informacao.codigo == codigo){
				printf("\n%d - %s\n",tmp->informacao.codigo,tmp->informacao.nome);
				printf("\nData:%s",tmp->informacao.data);
				printf("\nTitulos:%d",tmp->informacao.titulos);
				printf("\n\nEstado:%s",tmp->informacao.estado);
				printf("\nPresidente:%s",tmp->informacao.presidente);
			}
			tmp = tmp->prox;
		}
	}
	printf("\n\n'Aperte a tecla [ENTER] para voltar ao menu.'\n");
	n = getchar();
	__fpurge(stdin);
}
/*Pesquisa um item da lista dada como parametro e o remove da lista*/
void Remover(lista *l){
	no *tmp = l->inicio;
	no *p;
	printf("\nDigite o nome do time que deseja remover:");
	char c[30];
	LerString(c);
	__fpurge(stdin);
	int i = 0;
	int j = 0;
	if(l->qtde == 0){
		l->inicio = NULL;
		l->final = NULL;
		free(tmp);
	}
	while(c[i] != '\0'){
		i++;
	}
	int k = 0;
	while(tmp != NULL){
		j = 0;
		while(j < i){
			if(tmp->informacao.nome[k] == c[k])
				k++;
		j++;
		}
		if(k == i){
			p = tmp;
			if(tmp == l->inicio){
				tmp->prox->ant = NULL;
				l->inicio = tmp->prox;
				free(p);
			}
			else if(tmp == l->final){
				tmp->ant->prox = NULL;
				l->final = tmp->ant;
				free(p);
			}
			else{
				tmp->ant->prox = tmp->prox;
				tmp->prox->ant = tmp->ant;
				free(p);	
			}
			break;
		}
		tmp = tmp->prox;
	}
}
/*Pesquisa um item da lista dada como parametro e escolhe um dos atributos para modificar*/
void MudarInfo(lista *l){
	printf("\nDigite o nome do time que deseja fazer alterações:");
	char c[30];
	no *tmp = l->inicio;
	int escolha;
	LerString(c);
	__fpurge(stdin);
	char g;
	int i = 0;
	int j = 0;
	while(c[i] != '\0'){
		i++;
	}
	int k = 0;
	while(tmp != NULL){
		j = 0;
		while(j < i){
			if(tmp->informacao.nome[k] == c[k])
				k++;
		j++;
		}
		if(k == i){
			while(1){
				printf("\nQual campo deseja alterar:\n\n1-Nome.\n2-Data.\n3-Quantidade de titulos.\n4-Estado de origem.\n5-Presidente do clube.\n");
				printf("->");
				scanf("%d",&escolha);
				__fpurge(stdin);
				if(escolha == 1){
					printf("\nDigite o novo nome desejado:");
					LerString(tmp->informacao.nome);
					__fpurge(stdin);
				}
				if(escolha == 2){
					printf("\nDigite a nova data desejada:");
					fgets(tmp->informacao.data,10,stdin);
					__fpurge(stdin);
				}
				if(escolha == 3){
					printf("\nDigite a nova quantidade de titulos desejada:");
					scanf("%d",&tmp->informacao.titulos);
					__fpurge(stdin);
				}
				if(escolha == 4){
					printf("\nDigite o novo estado desejado:");
					fgets(tmp->informacao.estado,3,stdin);
					__fpurge(stdin);
				}
				if(escolha == 5){
					printf("\nDigite o novo nome do presidente desejado:");
					fgets(tmp->informacao.presidente,30,stdin);
					__fpurge(stdin);
				}
			system("clear");
			printf("\nDeseja Alterar outro campo?\n\n'Digite [s] para ""sim"" e [n] para ""não""\n");
			scanf("%c",&g);
			__fpurge(stdin);
			if(g == 'n')
				break;
			}
			break;
		}
		tmp = tmp->prox;
	}
}
/*Remove o item do inicio da lista dada como parametro*/
void RemoverInicio(lista *l){
	no *tmp = l->inicio;
	if(l->inicio == l->final){
		l->inicio = NULL;
		l->final = NULL;
	}
	else{
		l->inicio->prox->ant = NULL;
		l->inicio = l->inicio->prox;	
	}
	free(tmp);
}
