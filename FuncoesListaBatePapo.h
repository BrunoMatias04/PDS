#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>

void InserirLista_batepapo(listaBatePapo *l){
	noBatePapo *p = (noBatePapo*)malloc(sizeof(noBatePapo));
	printf("\nNome da sala: ");
	__fpurge(stdin);
	LerString_batepapo(p->informacao.nome_sala);
	__fpurge(stdin);
	printf("\nCategoria: ");
	__fpurge(stdin);
	LerString_batepapo(p->informacao.categoria);
	__fpurge(stdin);
	p->informacao.codigo = l->qtde;
	printf("\nCodigo da sala é: %d\n",p->informacao.codigo);
	printf("\nMax de pessoas: ");
	__fpurge(stdin);
	scanf("%d",&p->informacao.max_pessoas);
	__fpurge(stdin);
	p->prox = l->inicio;
	p->ant = NULL;
	l->inicio = p;
	if(l->final == NULL)
		l->final = l->inicio;
	else
		l->inicio->prox->ant = p;
}
void ImprimeTodos_batepapo(listaBatePapo *l){
	noBatePapo *p = l->inicio;
	while(p != NULL){
		printf("--------------------------------------------------------------------------------");
		printf("\n%d  -  %s\n",p->informacao.codigo,p->informacao.nome_sala);
		p = p->prox;
	}
	printf("--------------------------------------------------------------------------------");
	printf("\n");
}
void ImprimeEspecifico_batepapo(listaBatePapo *l){
	noBatePapo *tmp = l->inicio;
	char c[30];
	char n;
	int escolha;
	printf("\nGostaria de pesquisar por:\n1-Nome da sala.\n2-Codigo da sala.\n3-Sair\n");
	printf("Escolha: ");
	scanf("%d",&escolha);
	__fpurge(stdin);
	if(escolha == 1){
		printf("\nDigite o nome da sala desejada: ");
		LerString_batepapo(c);
		__fpurge(stdin);
		int i = 0;
		int j = 0;
		while(c[i] != '\0'){
			i++;
		}
		int k = 0;
		while(tmp != NULL){
			j = 0;
			while(j < i){
				if(tmp->informacao.nome_sala[k] == c[k])
					k++;
			j++;
			}
			if(k == i){
				printf("\n%d - %s\n",tmp->informacao.codigo,tmp->informacao.nome_sala);
				printf("\nCategoria: %s",tmp->informacao.categoria);
				printf("\n\nMaximo de pessoas: %d",tmp->informacao.max_pessoas);
				break;
			}
			tmp = tmp->prox;
		}

		printf("\n\n'Aperte a tecla [ENTER] para voltar ao menu.'\n");
		n = getchar();
		__fpurge(stdin);
	}
	else if(escolha == 2){
		int codigo;
		printf("\nDigite o codigo da sala desejada: ");
		scanf("%d",&codigo);
		__fpurge(stdin);
		while(tmp != NULL){
			if(tmp->informacao.codigo == codigo){
				printf("\n%d - %s\n",tmp->informacao.codigo,tmp->informacao.nome_sala);
				printf("\nCategoria: %s",tmp->informacao.categoria);
				printf("\n\nMaximo de pessoas: %d",tmp->informacao.max_pessoas);
			}
			tmp = tmp->prox;
		}
		printf("\n\n'Aperte a tecla [ENTER] para voltar ao menu.'\n");
		n = getchar();
		__fpurge(stdin);
	}
	else if(escolha == 3)
	{
		__fpurge(stdin);
	}

}
void Remover_batepapo(listaBatePapo *l){
	noBatePapo *tmp = l->inicio;
	noBatePapo *p;
	printf("\nDigite o nome da sala que deseja remover: ");
	char c[30];
	LerString_batepapo(c);
	__fpurge(stdin);
	int i = 0;
	int j = 0;
	if(l->qtde == 0){
		l->inicio = NULL;
		l->final = NULL;
		free(tmp);
	}
	while(c[i] != '\0'){
		i++;
	}
	int k = 0;
	while(tmp != NULL){
		j = 0;
		while(j < i){
			if(tmp->informacao.nome_sala[k] == c[k])
				k++;
		j++;
		}
		if(k == i){
			p = tmp;
			if(tmp == l->inicio){
				tmp->prox->ant = NULL;
				l->inicio = tmp->prox;
				free(p);
			}
			else if(tmp == l->final){
				tmp->ant->prox = NULL;
				l->final = tmp->ant;
				free(p);
			}
			else{
				tmp->ant->prox = tmp->prox;
				tmp->prox->ant = tmp->ant;
				free(p);
			}
			break;
		}
		tmp = tmp->prox;
	}
}
void MudarInfo_batepapo(listaBatePapo *l){


	printf("\nDigite o nome da sala que deseja fazer alterações: ");
	char c[30];
	noBatePapo *tmp = l->inicio;
	int escolha;
	LerString_batepapo(c);
	__fpurge(stdin);
	char g;
	int i = 0;
	int j = 0;
	while(c[i] != '\0'){
		i++;
	}
	int k = 0;
	while(tmp != NULL){
		j = 0;
		while(j < i){
			if(tmp->informacao.nome_sala[k] == c[k])
				k++;
		j++;
		}
		if(k == i){
			while(1){
				printf("\nQual campo deseja alterar:\n\n1-Nome da sala.\n2-Categoria.\n3-Quantidade maxima de pessoas.\n4-Todos\n5-Sair\n\n");
				printf("->");
				scanf("%d",&escolha);
				__fpurge(stdin);

				if(escolha == 1){
					printf("\nDigite o novo nome desejado: ");
					LerString_batepapo(tmp->informacao.nome_sala);
					__fpurge(stdin);
					system("clear");
					printf("\nDeseja Alterar outro campo?\n\n'Digite [s] para ""sim"" e [n] para ""não""\n");
					scanf("%c",&g);
					__fpurge(stdin);
					if(g == 'n')
						break;
					}

				if(escolha == 2){
					printf("\nDigite a nova categoria desejada: ");
					fgets(tmp->informacao.categoria,10,stdin);
					__fpurge(stdin);
					system("clear");
					printf("\nDeseja Alterar outro campo?\n\n'Digite [s] para ""sim"" e [n] para ""não""\n");
					scanf("%c",&g);
					__fpurge(stdin);
					if(g == 'n')
						break;
					}
				if(escolha == 3){
					printf("\nDigite a nova quantidade maxima de pessoas: ");
					scanf("%d",&tmp->informacao.max_pessoas);
					__fpurge(stdin);
					system("clear");
					printf("\nDeseja Alterar outro campo?\n\n'Digite [s] para ""sim"" e [n] para ""não""\n");
					scanf("%c",&g);
					__fpurge(stdin);
					if(g == 'n')
						break;
					}

				if(escolha==4){
					printf("\nDigite o novo nome desejado: ");
					LerString_batepapo(tmp->informacao.nome_sala);
					__fpurge(stdin);
					printf("\nDigite a nova categoria desejada: ");
					fgets(tmp->informacao.categoria,10,stdin);
					__fpurge(stdin);
					printf("\nDigite a nova quantidade maxima de pessoas: ");
					scanf("%d",&tmp->informacao.max_pessoas);
					__fpurge(stdin);
				}
				if(escolha==5){
					__fpurge(stdin);
					break;
				}

			}
			break;
		}
		tmp = tmp->prox;
	}
}
void RemoverInicio_batepapo(listaBatePapo *l){
	noBatePapo *tmp = l->inicio;
	if(l->inicio == l->final){
		l->inicio = NULL;
		l->final = NULL;
	}
	else{
		l->inicio->prox->ant = NULL;
		l->inicio = l->inicio->prox;
	}
	free(tmp);
}
