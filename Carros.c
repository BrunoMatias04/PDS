#include "StructListaCarros.h"
#include "FuncoesGeraisCarros.h"
#include "FuncoesListaCarros.h"
#include "FuncoesSalvar_e_RecuperarCarros.h"
int Carros(){
	int escolha;
	listaCarros *l = (listaCarros*)malloc(sizeof(listaCarros));
	l->qtde = 0;
	l->inicio = NULL;
	l->final = NULL;
	RecuperarCarros(l);
	while(1){
		system("clear");
		printf("\n1-Cadastrar Carro.\n2-Consultar Carro.\n3-Alterar informações do Carro.\n4-Remover Carro.\n5-Voltar ao menu\n\n");
		printf("Escolha uma opcao: ");
		scanf("%d",&escolha);
		__fpurge(stdin);
		if(escolha == 1){
			system("clear");
			l->qtde++;
			InserirListaCarros(l);
			SalvarCarros(l);
			RecuperarCarros(l);
		}
		else if(escolha == 4){
			system("clear");
			l->qtde--;
			RemoverCarros(l);
			SalvarCarros(l);
			RecuperarCarros(l);
		}
		else if(escolha == 3){
			system("clear");
			MudarInfoCarros(l);
			SalvarCarros(l);
			RecuperarCarros(l);
		}
		else if(escolha == 2){
			system("clear");
			ImprimeTodosCarros(l);
			ImprimeEspecificoCarros(l);
		}
		else if(escolha == 5){
			break;
		}
	}
	return 0;
}
